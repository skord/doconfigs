#!/usr/bin/env bash
apt-get install -y git postgresql-9.1 vim openjdk-7-jre-headless
echo "host 	all		all 		10.128.0.0/16	md5" >> /etc/postgresql/9.1/main/pg_hba.conf
/etc/init.d/postgresql restart
mkdir -p /opt/local/ruby
curl -O -L http://jruby.org.s3.amazonaws.com/downloads/1.7.4/jruby-bin-1.7.4.tar.gz
tar xvfz jruby-bin-1.7.4.tar.gz -C /opt/local/ruby
export PATH=/opt/local/ruby/jruby-1.7.4/bin:$PATH
echo "export PATH=/opt/local/ruby/jruby-1.7.4/bin:$PATH" >> ~/.bashrc
rm jruby-bin-1.7.4.tar.gz
echo "export JRUBY_OPTS='--1.9 --server -Xinvokedynamic.constants=true'" >> .bashrc
source ~/.bashrc
git clone https://skord@bitbucket.org/skord/stacktest.git
cd stacktest
gem install bundler
bundle
RAILS_ENV=production rake db:create
RAILS_ENV=production rake db:migrate
RAILS_ENV=production rake db:seed
RAILS_ENV=production rake tmp:create
# change listen addy somewhere...
