#!/usr/bin/env bash
apt-get -y install nginx-full git redis-server openjdk-7-jre-headless
mkdir -p /opt/local/ruby
curl -O -L http://jruby.org.s3.amazonaws.com/downloads/1.7.4/jruby-bin-1.7.4.tar.gz
tar xvfz jruby-bin-1.7.4.tar.gz -C /opt/local/ruby
export PATH=/opt/local/ruby/jruby-1.7.4/bin:$PATH
echo "export PATH=/opt/local/ruby/jruby-1.7.4/bin:$PATH" >> ~/.bashrc
rm jruby-bin-1.7.4.tar.gz
echo "export JRUBY_OPTS='--1.9 --server -Xinvokedynamic.constants=true'" >> .bashrc
source ~/.bashrc
# copy security jars
git clone https://skord@bitbucket.org/skord/stacktest.git
cd stacktest
gem install bundler
bundle
RAILS_ENV=production rake tmp:create
mkdir -p /var/www
cd ..
mv stacktest /var/www
cd /var
chown -R www-data:www-data www
cd /var/www/stacktest
bundle exec puma -e production -b unix:///var/www/stacktest/tmp/sockets/.puma.sock -t 8:16
